package com.classpath.inventorymicroservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderProcessor {

    @KafkaListener(topics = {"orders-topic-new"}, groupId = "pradeep-inventory-microservice")
    public void processOrder(String data){
        log.info("Processing data :: {} ", data);
    }

}